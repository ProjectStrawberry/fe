package io.loyloy.fe;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import io.loyloy.fe.database.Account;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@CommandAlias("fe")
public class FeCommand extends BaseCommand {
    Fe plugin;

    FeCommand(Fe p) {
        plugin = p;
    }

    @CommandAlias("balance|bal|money")
    @Subcommand("balance|bal|money")
    @Description("View a players balance")
    @CommandPermission("fe.balance")
    @CommandCompletion("@players")
    public void balance(CommandSender sender, @Optional OfflinePlayer targetPlayer) {
        Account account;

        if (targetPlayer == null && !(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Please specify a player when using this command from the console.");
            return;
        }

        if (targetPlayer != null && sender.hasPermission("fe.balance.other")) {
            account = plugin.getAPI().getAccount(targetPlayer.getName(), targetPlayer.getUniqueId().toString());

            if (account == null) {
                Phrase.ACCOUNT_DOES_NOT_EXIST.sendWithPrefix(sender);
                return;
            }

            Phrase.ACCOUNT_HAS.sendWithPrefix(sender, account.getName(), plugin.getAPI().format(account));
        } else {
            Player player = (Player) sender;
            account = plugin.getAPI().getAccount(player.getName(), player.getUniqueId().toString());

            if (account == null) {
                Phrase.YOUR_ACCOUNT_DOES_NOT_EXIST.sendWithPrefix(player);
                return;
            }

            Phrase.YOU_HAVE.sendWithPrefix(player, plugin.getAPI().format(account));
        }
    }

    @Subcommand("clean")
    @Description("Removes all accounts with the default balance")
    @CommandPermission("fe.clean")
    public void clean(CommandSender sender) {
        plugin.getAPI().clean();
        Phrase.ACCOUNT_CLEANED.sendWithPrefix(sender);
    }

    @Subcommand("create")
    @Description("Creates an account with the default balance")
    @CommandPermission("fe.create")
    @CommandCompletion("<name>")
    public void create(CommandSender sender, @Single String name) {
        if (plugin.getAPI().accountExists(name, null)) {
            Phrase.ACCOUNT_EXISTS.sendWithPrefix(sender);
            return;
        }

        if (name.length() > 16) {
            Phrase.NAME_TOO_LONG.sendWithPrefix(sender);
            return;
        }

        Account account = plugin.getAPI().updateAccount(name, null);
        Phrase.ACCOUNT_CREATED.sendWithPrefix(sender, Phrase.PRIMARY_COLOR.parse() + account.getName() + Phrase.SECONDARY_COLOR.parse());
    }

    @Subcommand("deduct")
    @Description("Deducts money from an account")
    @CommandPermission("fe.deduct")
    @CommandCompletion("@accounts <amount>")
    public void deduct(CommandSender sender, @Single String targetPlayer, double amount) {
        Account victim = plugin.getShortenedAccount(targetPlayer);
        if (victim == null) {
            Phrase.ACCOUNT_DOES_NOT_EXIST.sendWithPrefix(sender);
            return;
        }

        String formattedMoney = plugin.getAPI().format(amount);

        victim.withdraw(amount);

        Phrase.PLAYER_DEDUCT_MONEY.sendWithPrefix(sender, formattedMoney, victim.getName());
        Player receiverPlayer = plugin.getServer().getPlayerExact(victim.getName());

        if (receiverPlayer != null) {
            Phrase.PLAYER_DEDUCTED_MONEY.sendWithPrefix(receiverPlayer, formattedMoney, sender.getName());
        }
    }

    @Subcommand("grant")
    @Description("Gives a player money")
    @CommandPermission("fe.grant")
    @CommandCompletion("@accounts <amount> <silent>")
    public void grant(CommandSender sender, @Single String targetPlayer, double amount, @Optional String silent) {
        Account victim = plugin.getShortenedAccount(targetPlayer);

        if (victim == null) {
            Phrase.ACCOUNT_DOES_NOT_EXIST.sendWithPrefix(sender);
            return;
        }

        if (!victim.canReceive(amount)) {
            Phrase.MAX_BALANCE_REACHED.sendWithPrefix(sender, victim.getName());
            return;
        }

        String formattedMoney = plugin.getAPI().format(amount);

        victim.deposit(amount);

        if (silent == null || !silent.equals("true")) Phrase.PLAYER_GRANT_MONEY.sendWithPrefix(sender, formattedMoney, victim.getName());

        Player receiverPlayer = plugin.getServer().getPlayerExact(victim.getName());

        if (receiverPlayer != null && (silent == null || !silent.equals("true"))) {
            Phrase.PLAYER_GRANTED_MONEY.sendWithPrefix(receiverPlayer, formattedMoney, sender.getName());
        }
    }

    @Subcommand("reload")
    @Description("Reloads the configuration files")
    @CommandPermission("fe.reload")
    public void reload(CommandSender sender) {
        plugin.reloadConfig();
        Phrase.CONFIG_RELOADED.sendWithPrefix(sender);
    }

    @Subcommand("remove")
    @Description("Removes an account from the economy database")
    @CommandPermission("fe.remove")
    @CommandCompletion("<player>")
    public void remove(CommandSender sender, OfflinePlayer targetPlayer) {
        if (!plugin.getAPI().accountExists(targetPlayer.getName(), targetPlayer.getUniqueId().toString())) {
            Phrase.ACCOUNT_DOES_NOT_EXIST.sendWithPrefix(sender);
            return;
        }

        plugin.getAPI().removeAccount(targetPlayer.getName(), targetPlayer.getUniqueId().toString());
        Phrase.ACCOUNT_REMOVED.sendWithPrefix(sender, Phrase.PRIMARY_COLOR.parse() + targetPlayer.getName() + Phrase.SECONDARY_COLOR.parse());
    }

    @CommandAlias("pay")
    @Subcommand("send|pay|give")
    @Description("Sends a player money")
    @CommandPermission("fe.send")
    @CommandCompletion("@players <amount>")
    public void send(Player player, OfflinePlayer targetPlayer, double amount) {
        if (amount <= 0.0) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Please specify an amount greater than 0");
            return;
        }

        Account receiver = plugin.getAPI().getAccount(targetPlayer.getName(), targetPlayer.getUniqueId().toString());

        if (receiver == null) {
            Phrase.ACCOUNT_DOES_NOT_EXIST.sendWithPrefix(player);
            return;
        }

        Account account = plugin.getAPI().getAccount(player.getName(), player.getUniqueId().toString());

        if (!account.has(amount)) {
            Phrase.NOT_ENOUGH_MONEY.sendWithPrefix(player);
            return;
        }

        if (!receiver.canReceive(amount)) {
            Phrase.MAX_BALANCE_REACHED.sendWithPrefix(player, receiver.getName());
            return;
        }

        String formattedMoney = plugin.getAPI().format(amount);

        account.withdraw(amount);
        receiver.deposit(amount);

        Phrase.MONEY_SENT.sendWithPrefix(player, formattedMoney, receiver.getName());
        Player receiverPlayer = plugin.getServer().getPlayerExact(receiver.getName());

        if (receiverPlayer != null) {
            Phrase.MONEY_RECEIVE.sendWithPrefix(receiverPlayer, formattedMoney, player.getName());
        }
    }

    @Subcommand("set")
    @Description("Sets a players balance")
    @CommandPermission("fe.set")
    @CommandCompletion("@players <amount>")
    public void set(CommandSender sender, @Single String targetPlayer, double amount) {
        Account victim = plugin.getShortenedAccount(targetPlayer);

        if (victim == null) {
            Phrase.ACCOUNT_DOES_NOT_EXIST.sendWithPrefix(sender);
            return;
        }

        if (!victim.canReceive(amount)) {
            Phrase.MAX_BALANCE_REACHED.sendWithPrefix(sender, victim.getName());
            return;
        }

        String formattedMoney = plugin.getAPI().format(amount);
        victim.setMoney(amount);
        Phrase.PLAYER_SET_MONEY.sendWithPrefix(sender, victim.getName(), formattedMoney);
    }

    @Subcommand("top")
    @Description("Displays the richest players on the server")
    @CommandPermission("fe.top")
    public void top(CommandSender sender) {
        List<Account> topAccounts = plugin.getAPI().getTopAccounts();

        if (topAccounts.size() < 1) {
            Phrase.NO_ACCOUNTS_EXIST.sendWithPrefix(sender);
            return;
        }

        sender.sendMessage(plugin.getEqualMessage(Phrase.RICH.parse(), 10));

        for (int i = 0; i < topAccounts.size(); i++) {
            Account account = topAccounts.get(i);

            String two = Phrase.SECONDARY_COLOR.parse();

            sender.sendMessage(two + (i + 1) + ". " + Phrase.PRIMARY_COLOR.parse() + account.getName() + two + " - " + plugin.getAPI().format(account));
        }

        sender.sendMessage(plugin.getEndEqualMessage(28));
    }

    @HelpCommand
    public static void onHelp(CommandHelp help) {
        help.showHelp();
    }
}
